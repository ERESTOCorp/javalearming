
public class Lesson2 {
	
	public static void printArray(int[] array) {
		for(byte i = 0; i < array.length; i++) {
			System.out.print(array[i]+" ");
		}
		System.out.println();
	}
	
	public static int[] quickSort(int start, int end, int[] array) {
		if(end < start)
			return array;
		int center = array[array.length/2];
		int i = start,j = end - 1;
		while(i <= j) {
			while(array[i] < center)
				i++;
			while(array[j] > center)
				j--;
			if(i < j) {
				array[i] += array[j] - (array[j] = array[i]);
				i++;
				j--;
			}
		}
		if(i < end)
			array = quickSort(i, end - i, array);
		if(j > 0)
			array = quickSort(0, j + 1, array);
		return array;
	}
	
	public static int[] quickSort(int[] array) {
		return quickSort(0, array.length, array);
	}
	
	public static void main(String[] args) {
		int[] unsortedArr = {5,3,4,0,1};
		int[] sortedArr = quickSort(unsortedArr);
		printArray(sortedArr);
	}

}
